package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
)

func connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

func read_msg(reader *bufio.Reader) (msg interface{}, err error, line_return string) {
	var line string
	line, err = reader.ReadString('\n')
//	log.Println("line ", line);
	if err != nil {
		return
	}
	err = json.Unmarshal([]byte(line), &msg)
	if err != nil {
		return
	}
	return msg, err, line
}

func write_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	m := make(map[string]interface{})
	m["msgType"] = msgtype
	m["data"] = data
	var payload []byte
	payload, err = json.Marshal(m)
	_, err = writer.Write([]byte(payload))
	if err != nil {
		return
	}
	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}
	writer.Flush()
	return
}

func send_join(writer *bufio.Writer, name string, key string) (err error) {
	data := make(map[string]string)
	data["name"] = name
	data["key"] = key
	err = write_msg(writer, "join", data)
	return
}

func send_ping(writer *bufio.Writer) (err error) {
	err = write_msg(writer, "ping", make(map[string]string))
	return
}

func send_throttle(writer *bufio.Writer, throttle float32) (err error) {
	err = write_msg(writer, "throttle", throttle)
	return
}
//evil mutable state.
var current_throttle float32= 1.0;

//track array

func dispatch_msg(writer *bufio.Writer, msgtype string, data interface{}, line string) (err error) {
//	log.Println("data: ", data);
	switch msgtype {
	case "join":
		log.Printf("Joined")
		send_ping(writer)
	case "gameStart":
		log.Printf("Game started")
		send_throttle(writer, 1.0)
	case "crash":
		log.Printf("Someone crashed")
		send_ping(writer)
	case "gameEnd":
		log.Printf("Game ended")
		send_ping(writer)
	case "carPositions":
		//angle := get_angle(line)
		//send_throttle(writer, adjust_throttle(angle, current_throttle))
		carPositionMsg := get_car_position_msg(line)
		send_throttle(writer,adjust_throttle_by_track(carPositionMsg))
	case "error":
		log.Printf(fmt.Sprintf("Got error: %v", data))
		send_ping(writer)
	case "gameInit":	
		log.Printf("gameInit")
		save_track(line)
	case "yourCar":
		log.Printf("yourCar")
		
		save_yourcar(line)
		
		
	default:
		log.Printf("Got msg type: %s", msgtype)
		send_ping(writer)
	}
	return
}

func parse_and_dispatch_input(writer *bufio.Writer, input interface{}, line string) (err error) {
	switch t := input.(type) {
	default:
		err = errors.New(fmt.Sprintf("Invalid message type: %T", t))
		return
	case map[string]interface{}:
		var msg map[string]interface{}
		var ok bool
		msg, ok = input.(map[string]interface{})
		if !ok {
			err = errors.New(fmt.Sprintf("Invalid message type: %v", msg))
			return
		}
		switch msg["data"].(type) {
		default:
			err = dispatch_msg(writer, msg["msgType"].(string), nil, "")
			if err != nil {
				return
			}
		case interface{}:
			err = dispatch_msg(writer, msg["msgType"].(string), msg["data"].(interface{}), line)
			if err != nil {
				return
			}
		}
	}
	return
}

func bot_loop(conn net.Conn, name string, key string) (err error) {
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	send_join(writer, name, key)
	for {
		input, err, line := read_msg(reader)
		if err != nil {
			log_and_exit(err)
		}
		err = parse_and_dispatch_input(writer, input, line)
		if err != nil {
			log_and_exit(err)
		}
	}
}

func parse_args() (host string, port int, name string, key string, err error) {
	args := os.Args[1:]
	if len(args) != 4 {
		return "", 0, "", "", errors.New("Usage: ./run host port botname botkey")
	}
	host = args[0]
	port, err = strconv.Atoi(args[1])
	if err != nil {
		return "", 0, "", "", errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1]))
	}
	name = args[2]
	key = args[3]

	return
}

func log_and_exit(err error) {
	log.Fatal(err)
	os.Exit(1)
}

func main() {
	host, port, name, key, err := parse_args()

	if err != nil {
		log_and_exit(err)
	}

	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, name, key)

	conn, err := connect(host, port)

	if err != nil {
		log_and_exit(err)
	}

	defer conn.Close()

	err = bot_loop(conn, name, key)
}


type CarPositionMsg struct {
	MsgType  string
	GameId   string
	GameTick int32
	Data    [] struct {
		Id struct {
			Name  string
			Color string
		}
		PiecePosition struct {
			PieceIndex      int32
			InPieceDistance float32
			Lane            struct {
				StartLaneIndex int32
				EndLaneIndex   int32
			}
			Lap int32
		}

		Angle float32
	}
}

// info about the track
var trackInfo TrackInfo
var trackPieces[] Pieces

var yourCar YourCarMsg // save the information about the car. Used for getting correct CarPosition later.

type Pieces struct {
					// single track piece					
					Angle *float32
					Radius *float32
					Length *float32
					Switch *bool						
}

// information about the complete track
type TrackInfo struct {
	Data struct {
		Race struct {
			Track struct {
				Id string
				Name string				
				// array containing all track pieces
				Pieces []Pieces
				// lanes & startingPoint are missing
			}
		}
	} 
}

// saves the track information from incomint track JSON
func save_track(data string) {
	log.Println("saving track")
	log.Println(data)
	src_json := []byte(data)
	
	msg := TrackInfo{}
	err := json.Unmarshal(src_json, &msg)
	if err != nil {
		panic(err)
	}
	trackInfo = msg
	trackPieces = msg.Data.Race.Track.Pieces
			
}

func get_car_position_msg(data string) CarPositionMsg {
	src_json := []byte(data)

	msg := CarPositionMsg{}
	err := json.Unmarshal(src_json, &msg)
	if err != nil {
		panic(err)
	}

  return msg //FIXME check the correct car.	
}


func get_angle(data string) float32 {
//TODO make sure you get the correct car from the data.
// inspiration from http://play.golang.org/p/4EaasS2VLL
	src_json := []byte(data)

	msg := CarPositionMsg{}
	err := json.Unmarshal(src_json, &msg)
	if err != nil {
		panic(err)
	}

  return msg.Data[0].Angle //FIXME check the correct car.
} 

func adjust_throttle_by_track(carPosition CarPositionMsg) float32 {
	
	// get current track piece index
	curIndex := get_current_track_piece_index(carPosition)
	// check the upcoming track pieces
	var nextPiece Pieces
	if (int32(len(trackPieces)) == int32(curIndex +1) ) {
		nextPiece = trackPieces[0]
	} else {
		nextPiece = trackPieces[curIndex +1]
	}
	currentPiece := trackPieces[curIndex]
	
	if (nextPiece.Radius != nil || currentPiece.Radius != nil) {
//		log.Println("carefully ")
		return 0.4// this OR next pieces are turns - drive carefully
	} else {
//		log.Println("full speed! ")
		return 0.75// FULL THROTTLE!
	}
}

func get_current_track_piece_index(carPosition CarPositionMsg) int {
// consider the different amount of cars as well

	cars := carPosition.Data
	for index, car := range cars  {
//		log.Println("car ", car.Id.Name, " index ", index )
	 	if yourCar.Data.Name == car.Id.Name {
//	 		log.Println("current track piece index, returning ", index)
		 	return index 
	 	}
	}
	return 0
}

func adjust_throttle(angle float32, throttle float32) float32 {
var MIN_THROTTLE float32 = 0.1
var MAX_THROTTLE float32 = 0.78

	new_throttle := throttle
	
	if( angle < 0 ) {
		angle = angle * -1 
	}
	if (angle < 10) {
		new_throttle = MAX_THROTTLE //throttle + 0.3
	} else if (angle >= 11 &&  angle <= 20 ) {
		new_throttle = MIN_THROTTLE//throttle + 0.2
	} else if (angle >= 21 &&  angle <= 30 ) {
		new_throttle = MIN_THROTTLE//throttle - 0.4
	} else if (angle >= 31 ) {
		new_throttle = MIN_THROTTLE//throttle -0.5
	} 

	//make sure the throttle does not increase over 1.0
	if (new_throttle > MAX_THROTTLE ) {
		new_throttle = MAX_THROTTLE
	} else if (new_throttle <= MIN_THROTTLE ) {
		new_throttle = MIN_THROTTLE
	}
	
	current_throttle = new_throttle
	log.Println("angle ", angle, " adjusted throttle ", new_throttle)
	return new_throttle;
}

//line  {"msgType":"yourCar","data":{"name":"Team Bitodi","color":"red"},
type YourCarMsg struct {
	MsgType  string
	Data  struct {
		Name  string
		Color string
	}
}


func save_yourcar(data string)  {
//TODO make sure you get the correct car from the data.
// inspiration from http://play.golang.org/p/4EaasS2VLL
	src_json := []byte(data)

	msg := YourCarMsg{}
	err := json.Unmarshal(src_json, &msg)
	if err != nil {
		panic(err)
	}

	yourCar = msg
	log.Println("Saved your car ", yourCar) 

} 

